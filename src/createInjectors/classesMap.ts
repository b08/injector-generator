import { ClassModel, TypeDefinition, TypeId } from "@b08/type-parser";
import { classes, getClassById, isImportableMonotype } from "@b08/type-parser-methods";
import { ParsedModel } from "../types";

export function classesToInject(parsed: ParsedModel[]): ClassModel[] {
  return classes(parsed, isClassToInject);
}

export function isClassToInject(cls: ClassModel): boolean {
  return cls.constructor.parameters.every(p => !p.isOptional); // no optional parameters
}

export function getClass(id: TypeId, parsed: ParsedModel[]): ClassModel {
  const cls = getClassById(id, parsed);
  return cls && isClassToInject(cls) ? cls : null;
}

export function getClassByType(type: TypeDefinition, parsed: ParsedModel[]): ClassModel {
  return isImportableMonotype(type) ? getClass(type.importedTypes[0].id, parsed) : null;
}
