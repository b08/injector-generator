import { classesToInject } from "./classesMap";
import { getInjector } from "./getInjectorForClass";
import { Injector, InjectorOptions } from "./types";

export function createInjectors(options: InjectorOptions): Injector[] {
  return classesToInject(options.parsed)
    .map(cls => getInjector(cls.id, options));
}
