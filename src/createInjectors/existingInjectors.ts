import { flatMap, last, trimOne } from "@b08/array";
import { KeyMap, mapBy } from "@b08/flat-key";
import { memoize } from "@b08/memoize";
import { FunctionModel, ParameterModel, TypeId } from "@b08/type-parser";
import { injectorConstants } from "../produce/injectorConstants.const";
import { getClassByType } from "./classesMap";
import { lowerFirstLetter } from "./lowerFirstLetter";
import { InjectorOptions } from "./types";
import { Injector } from "./types/injector.type";

export type InjectorsMap = KeyMap<TypeId, Injector>;

function isInjectorParameter(p: ParameterModel, options: InjectorOptions): boolean {
  return !p.isOptional // no optional parameters
    && getClassByType(p.parameterType, options.parsed) == null;
}

function isInstances(p: ParameterModel): boolean {
  return p.parameterName === injectorConstants.instancesVarName &&
    p.parameterType.typeName === injectorConstants.instancesVarType;
}

function isClassInjector(func: FunctionModel, options: InjectorOptions): boolean {
  const cls = getClassByType(func.returnType, options.parsed);
  return cls != null && lowerFirstLetter(cls.id.name) === func.id.name
    && areInjectorParameters(func.parameters, options);
}

function areInjectorParameters(parameters: ParameterModel[], options: InjectorOptions): boolean {
  return parameters.length > 0
    && trimOne(parameters).every(p => isInjectorParameter(p, options))
    && isInstances(last(parameters));
}

function createExistingInjector(func: FunctionModel): Injector {
  const parameters = trimOne(func.parameters);
  return {
    id: func.id,
    resolverId: func.id,
    classId: func.returnType.importedTypes[0].id,
    predefined: true,
    dependsOn: [],
    targetParameters: [],
    injectorParameters: parameters.map(p => ({ parameterName: p.parameterName, parameterType: p.parameterType }))
  };
}

const existingInjectors = memoize(function existingInjectors(options: InjectorOptions): InjectorsMap {
  const injectors = flatMap(options.parsed, p => p.functions)
    .filter(f => isClassInjector(f, options))
    .map(createExistingInjector);
  return mapBy(injectors, i => i.classId);
});

export function existingInjector(id: TypeId, options: InjectorOptions): Injector {
  const injectors = existingInjectors(options);
  const existing = injectors.get(id);
  return existing;
}
