import { TypeDefinition, TypeId } from "@b08/type-parser";
import { InjectionReplacement } from "../../types";
import { InjectorOptions } from "../types";
import { getNameReplacement } from "./getNameReplacement";
import { getTypeReplacement, getTypeReplacementById } from "./getTypeReplacement";

export function getReplacement(id: TypeId, toReplace: TypeDefinition, options: InjectorOptions): InjectionReplacement {
  return getTypeReplacement(id.folder, toReplace, options) ||
    getNameReplacement(id.folder, toReplace.typeName, options);
}

export function getReplacementById(id: TypeId, toReplace: TypeId, options: InjectorOptions): InjectionReplacement {
  return getTypeReplacementById(id.folder, toReplace, options) ||
    getNameReplacement(id.folder, toReplace.name, options);
}
