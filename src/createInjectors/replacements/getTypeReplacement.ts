import { memoize } from "@b08/memoize";
import { TypeDefinition, TypeId } from "@b08/type-parser";
import { InjectionReplacement } from "../../types";
import { InjectorOptions } from "../types";
import { getMatchingTypes } from "./getMatchingTypes";
import { groupBy } from "@b08/flat-key";
import { isImportableMonotype, getOriginalId } from "@b08/type-parser-methods";

export function getTypeReplacement(folder: string, def: TypeDefinition, options: InjectorOptions): InjectionReplacement {
  if (!isImportableMonotype(def)) { return null; }
  return getTypeReplacementById(folder, def.importedTypes[0].id, options);
}

export function getTypeReplacementById(folder: string, id: TypeId, options: InjectorOptions): InjectionReplacement {
  const map = replacementMap(folder, options);
  const orig = getOriginalId(id, options.parsed);
  const replacements = map.get(orig);
  return replacements && replacements[0];
}

const replacementMap = memoize((folder: string, options: InjectorOptions) =>
  groupBy(folderReplacements(folder, options), r => r.type));

const folderReplacements = memoize((folder: string, options: InjectorOptions) => {
  const types = getMatchingTypes(folder, options.typeReplacements, options);
  return options.typeReplacements.filter(r => types.has(r.replacement));
});
