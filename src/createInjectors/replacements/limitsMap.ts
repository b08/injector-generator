import { memoize } from "@b08/memoize";
import { InjectorOptions } from "../types";
import { mapBy } from "@b08/flat-key";

export const limitsMap = memoize((options: InjectorOptions) => mapBy(options.limits, l => l.type, l => l.limitToPath));
