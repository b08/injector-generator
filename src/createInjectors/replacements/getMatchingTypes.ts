import { InjectorOptions } from "../types";
import { ReplacementType } from "../..";
import { unique, KeySet } from "@b08/flat-key";
import { limitsMap } from "./limitsMap";
import { InjectionReplacement } from "../../types";

export function getMatchingTypes(folder: string, replacements: InjectionReplacement[], options: InjectorOptions): KeySet<ReplacementType> {
  const types = unique(replacements.map(r => r.replacement));
  const filtered = types.filter(t => {
    if (!limitsMap(options).has(t)) {
      return true;
    }
    const limit = limitsMap(options).get(t);
    return folder.match(limit) != null;
  });
  return new KeySet(filtered);
}
