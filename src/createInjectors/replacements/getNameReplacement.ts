import { memoize } from "@b08/memoize";
import { InjectionReplacement, NameReplacement } from "../..";
import { InjectorOptions } from "../types";
import { getMatchingTypes } from "./getMatchingTypes";

export function getNameReplacement(folder: string, name: string, options: InjectorOptions): InjectionReplacement {
  return folderReplacements(folder, options)
    .find(repl => isMatchingReplacement(repl, name));
}

function isMatchingReplacement(repl: NameReplacement, typeName: string): boolean {
  return typeName.match(repl.typeNameRegex) != null;
}

const folderReplacements = memoize((folder: string, options: InjectorOptions) => {
  const types = getMatchingTypes(folder, options.nameReplacements, options);
  return options.nameReplacements.filter(r => types.has(r.replacement));
});


