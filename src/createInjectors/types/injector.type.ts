import { TypeId, TypeDefinition } from "@b08/type-parser";

export function injector(id: TypeId, resolverId: TypeId, classId: TypeId, predefined: boolean, dependsOn: Injector[],
  injectorParameters: InjectorParameter[], targetParameters: TargetParameter[]): Injector {
  return { id, resolverId, classId, predefined, dependsOn, injectorParameters, targetParameters };
}

export interface Injector {
  id: TypeId;
  resolverId: TypeId;
  index?: number;
  classId: TypeId;
  predefined: boolean;
  dependsOn: Injector[];
  injectorParameters: InjectorParameter[];
  targetParameters: TargetParameter[];
}

export interface InjectorParameter {
  parameterName: string;
  parameterType: TypeDefinition;
}

export interface TargetParameter {
  name: string;
  resolve: string;
}
