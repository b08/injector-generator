import { NameReplacement, ParsedModel, TypeLimit, TypeReplacement } from "../../types";

export interface InjectorOptions {
  limits: TypeLimit[];
  typeReplacements: TypeReplacement[];
  nameReplacements: NameReplacement[];
  parsed: ParsedModel[];
}
