import { TypeId } from "@b08/type-parser";
import { lowerFirstLetter } from "./lowerFirstLetter";

export function createInjectorId(classId: TypeId): TypeId {
  return {
    folder: classId.folder,
    file: "injector",
    name: lowerFirstLetter(classId.name),
    isModulesPath: false
  };
}
