import { flatMap } from "@b08/array";
import { unique } from "@b08/flat-key";
import { ParameterModel, TypeId } from "@b08/type-parser";
import { getClass } from "./classesMap";
import { getInjectorForParameter } from "./getDependsOn";
import { getReplacement } from "./replacements/getReplacement";
import { Injector, InjectorOptions, InjectorParameter } from "./types";

export function getParametersToInject(id: TypeId, depends: Injector[], options: InjectorOptions): InjectorParameter[] {
  const newParameters = getClass(id, options.parsed).constructor.parameters
    .filter(parm => getInjectorForParameter(id, parm, options) == null)
    .map(p => createInjectorParameter(p));
  const injectorParameters = flatMap(depends, d => d.injectorParameters);
  return unique([...newParameters, ...injectorParameters]
    .map(parm => tryReplacingParameter(parm, id, options)),
    p => p.parameterType.typeName);
}

function createInjectorParameter(parameter: ParameterModel): InjectorParameter {
  return { parameterName: parameter.parameterName, parameterType: parameter.parameterType };
}

function tryReplacingParameter(parm: InjectorParameter, id: TypeId, options: InjectorOptions): InjectorParameter {
  const replacement = getReplacement(id, parm.parameterType, options);
  if (replacement == null) { return parm; }

  return {
    parameterName: replacement.parameterName,
    parameterType: {
      typeName: replacement.replacement.name,
      importedTypes: [{ alias: replacement.replacement.name, id: replacement.replacement }]
    }
  };
}

