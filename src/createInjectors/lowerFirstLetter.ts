export function lowerFirstLetter(name: string): string {
  const first = name[0].toLowerCase();
  const rest = name.substr(1);
  return first + rest;
}
