import { KeySet } from "@b08/flat-key";
import { memoize } from "@b08/memoize";
import { TypeId } from "@b08/type-parser";
import { createInjector } from "./createInjectorForClass";
import { existingInjector } from "./existingInjectors";
import { Injector, InjectorOptions } from "./types";
import { getOriginalId } from "@b08/type-parser-methods";

const set = new KeySet<TypeId>();
export function getInjector(id: TypeId, options: InjectorOptions): Injector {
  if (id == null) { return null; }
  return getInjectorById(set.getOrAdd(getOriginalId(id, options.parsed)), options);
}

const getInjectorById = memoize(function getInjector(id: TypeId, options: InjectorOptions): Injector {
  return existingInjector(id, options)
    || createInjector(id, options);
});

