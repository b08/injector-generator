import { TypeId } from "@b08/type-parser";
import { getClass } from "./classesMap";
import { getInjectorForType } from "./getDependsOn";
import { getReplacement } from "./replacements/getReplacement";
import { Injector, InjectorOptions, InjectorParameter, TargetParameter } from "./types";

export function createTargetParameters(id: TypeId, options: InjectorOptions): TargetParameter[] {
  return getClass(id, options.parsed).constructor.parameters
    .map((p, i) => ({
      name: `d${i}`,
      resolve: resolveParameter(id, p, options)
    }));
}

function resolveParameter(id: TypeId, parameter: InjectorParameter, options: InjectorOptions): string {
  const injector = getInjectorForType(id, parameter.parameterType, options);
  return injector != null
    ? injectorCall(id, injector, options)
    : resolveExternalParameter(id, parameter, options);
}

function resolveExternalParameter(id: TypeId, parameter: InjectorParameter, options: InjectorOptions): string {
  const replacement = getReplacement(id, parameter.parameterType, options);
  if (replacement == null) { return parameter.parameterName; }
  return replacement.parameterName
    + replacement.resolve.replace("{name}", parameter.parameterName);
}

function injectorCall(id: TypeId, injector: Injector, options: InjectorOptions): string {
  const injectorParameters = injector.injectorParameters
    .map(p => resolveParameter(id, p, options)).map(parm => parm + ", ").join("");
  return `${injector.resolverId.name}(${injectorParameters}instances)`;
}
