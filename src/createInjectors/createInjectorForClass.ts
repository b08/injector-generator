import { TypeId } from "@b08/type-parser";
import { getClass } from "./classesMap";
import { createInjectorId } from "./createInjectorId";
import { createTargetParameters } from "./createTargetParameter";
import { getDependsOn } from "./getDependsOn";
import { getParametersToInject } from "./getParametersToInject";
import { injector, Injector, InjectorOptions } from "./types";
import { injectorConstants } from "../produce/injectorConstants.const";

export function createInjector(id: TypeId, options: InjectorOptions): Injector {
  const cls = getClass(id, options.parsed);
  if (cls == null) { return null; }
  const injectorId = createInjectorId(id);
  const resolverId = createResolverId(injectorId);
  const depends = getDependsOn(id, options);
  const inject = getParametersToInject(id, depends, options);
  const target = createTargetParameters(id, options);
  return injector(injectorId, resolverId, id, false, depends, inject, target);
}

function createResolverId(injectorId: TypeId): TypeId {
  return { ...injectorId, name: injectorId.name + injectorConstants.resolverSuffix };
}
