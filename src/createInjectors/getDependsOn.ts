import { ParameterModel, TypeId, TypeDefinition } from "@b08/type-parser";
import { isImportableMonotype } from "@b08/type-parser-methods";
import { getClass } from "./classesMap";
import { getInjector } from "./getInjectorForClass";
import { getReplacement } from "./replacements/getReplacement";
import { Injector, InjectorOptions } from "./types";

export function getDependsOn(id: TypeId, options: InjectorOptions): Injector[] {
  return getClass(id, options.parsed).constructor.parameters
    .map(parm => getInjectorForParameter(id, parm, options))
    .filter(i => i != null);
}

export function getInjectorForParameter(id: TypeId, parm: ParameterModel, options: InjectorOptions): Injector {
  return getInjectorForType(id, parm.parameterType, options);
}

export function getInjectorForType(id: TypeId, type: TypeDefinition, options: InjectorOptions): Injector {
  const replacement = getReplacement(id, type, options);
  if (replacement) { return getInjector(replacement.replacement, options); }
  if (!isImportableMonotype(type)) { return null; }
  return getInjector(type.importedTypes[0].id, options);
}
