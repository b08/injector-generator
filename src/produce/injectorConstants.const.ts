export const injectorConstants = {
  resolverSuffix: "Resolver",
  instancesVarName: "instances",
  instancesVarType: "any",
  maxLineLength: 140
};
