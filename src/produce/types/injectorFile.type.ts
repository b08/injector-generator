import { ImportModel } from "@b08/imports-generator";

export interface InjectorFile {
  imports: ImportModel[];
  injectors: ProducedInjector[];
}

export interface ProducedInjector {
  name: string;
  className: string;
  injectorParametersDeclare: string;
  targetParameters: ProducedTargetParameter[];
  targetParametersLine: string;
  resolverName: string;
  resolverIndex: string;
  resolverParameters: string;
  resolverParametersDeclare: string;
}

export interface ProducedTargetParameter {
  name: string;
  inPlace: boolean;
  resolve: string;
}
