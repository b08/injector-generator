import { Injector } from "../createInjectors/types";
import { ProducedTargetParameter } from "./types";
import { INumberMap } from "@b08/object-map";
import { injectorConstants } from "./injectorConstants.const";


export function produceTargetParameters(injector: Injector): ProducedTargetParameter[] {
  const allowedIndexes = getAllowedIndexMap(injector);
  return injector.targetParameters.map((parm, i) => ({ ...parm, inPlace: !!allowedIndexes[i] }));
}

function getAllowedIndexMap(injector: Injector): INumberMap<boolean> {
  // length of "  return new @className();" and colons
  const lengthLimit = injectorConstants.maxLineLength - injector.classId.name.length - 11 - 2 * (injector.targetParameters.length - 1);
  const lengths = injector.targetParameters
    .map((parm, index) => ({ length: parm.resolve.length - parm.name.length, index }))
    .sort((l1, l2) => l1.length - l2.length);
  let paramLength = 0;
  const map = {};
  for (let i = 0; i < lengths.length; i++) {
    paramLength += lengths[i].length;
    if (paramLength > lengthLimit) {
      break;
    }

    map[lengths[i].index] = true;
  }
  return map;
}

