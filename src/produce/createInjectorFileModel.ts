import { flatMap } from "@b08/array";
import { ImportModel } from "@b08/imports-generator";
import { importIds, importTypeDefinitions } from "@b08/type-parser-methods";
import { Injector } from "../createInjectors/types";
import { produceTargetParameters } from "./produceTargetParameters";
import { InjectorFile, ProducedInjector } from "./types";

export function createInjectorFileModel(injectors: Injector[]): InjectorFile {
  return {
    imports: produceImports(injectors),
    injectors: produceInjectors(injectors)
  };
}

function produceImports(injectors: Injector[]): ImportModel[] {
  return flatMap(injectors, produceInjectorImports);
}

function produceInjectorImports(injector: Injector): ImportModel[] {
  const injectors = importIds(injector.dependsOn.map(d => d.resolverId), injector.id);
  const dependencies = importTypeDefinitions(injector.injectorParameters.map(p => p.parameterType), injector.id);

  const classImport = {
    type: injector.classId.name,
    alias: injector.classId.name,
    importPath: `./${injector.classId.file}`
  };
  return [...injectors, classImport, ...dependencies];
}

function produceInjectors(injectors: Injector[]): ProducedInjector[] {
  return injectors.map(produceInjector);
}

function produceInjector(injector: Injector): ProducedInjector {
  const parameters = injector.injectorParameters
    .map(parm => `${parm.parameterName}: ${parm.parameterType.typeName}`);
  const injectorParametersLine = parameters.join(", ");
  const resolverParametersLine = parameters.map(p => p + ", ").join("");
  const resolverParameters = injector.injectorParameters.map(p => p.parameterName + ", ").join("");
  const targetParameters = produceTargetParameters(injector);
  const targetParametersLine = targetParameters.map(p => p.inPlace ? p.resolve : p.name).join(", ");
  return {
    name: injector.id.name,
    className: injector.classId.name,
    injectorParametersDeclare: injectorParametersLine,
    targetParameters,
    targetParametersLine,
    resolverName: injector.resolverId.name,
    resolverParametersDeclare: resolverParametersLine,
    resolverIndex: injector.index.toString(),
    resolverParameters
  };
}

