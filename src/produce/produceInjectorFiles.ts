import { Injector } from "../createInjectors/types";
import { ContentFile, GeneratorOptions } from "../types";
import { groupBy } from "@b08/flat-key";
import { injectorFile } from "./razor/injectorFile.rzr";
import { createInjectorFileModel } from "./createInjectorFileModel";
import { disclaimer } from "../disclaimer/disclaimer";

export function produceInjectorFiles(injectors: Injector[], options: GeneratorOptions): ContentFile[] {
  const newInjectors = injectors.filter(i => !i.predefined)
    .map((i, index) => ({ ...i, index }));
  const grouped = groupBy(newInjectors, i => ({ file: i.id.file, folder: i.id.folder }));
  return grouped.values().map(v => produceInjectorFile(v, options));
}

function produceInjectorFile(injectors: Injector[], options: GeneratorOptions): ContentFile {
  const model = createInjectorFileModel(injectors);
  return {
    folder: injectors[0].id.folder,
    name: injectors[0].id.file,
    contents: disclaimer(options) + injectorFile.generate(model, options),
    extension: ".ts"
  };
}
