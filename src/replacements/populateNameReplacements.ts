import { ParsedModel, NameReplacement } from "../types";
import { flatMap } from "@b08/array";
import { ClassModel } from "@b08/type-parser";
import { createParameterName } from "./createParameterName";

export function populateNameReplacements(parsed: ParsedModel[]): NameReplacement[] {
  return flatMap(parsed, p => p.classes)
    .filter(isReplacement)
    .map(createReplacement);
}

const regex = /^replacement for ([^\s]+)\s*$/;

const isReplacementComment = (comment: string) => comment.match(regex) != null;
function isReplacement(cls: ClassModel): boolean {
  return cls.comments.some(isReplacementComment);
}

function createReplacement(cls: ClassModel): NameReplacement {
  const comment = cls.comments.find(isReplacementComment);
  const name = comment.match(regex)[1];
  return {
    typeNameRegex: new RegExp(`^${name}$`),
    replacement: cls.id,
    resolve: "",
    parameterName: createParameterName(cls.id)
  };
}
