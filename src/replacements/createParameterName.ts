import { TypeId } from "@b08/type-parser";
import { lowerFirstLetter } from "../createInjectors/lowerFirstLetter";

export function createParameterName(type: TypeId): string {
  return type.name.startsWith("I")
    ? lowerFirstLetter(type.name.substr(1))
    : lowerFirstLetter(type.name);
}
