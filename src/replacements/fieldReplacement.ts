import { FieldModel, InterfaceModel } from "@b08/type-parser";
import { getOriginalId } from "@b08/type-parser-methods";
import { ParsedModel, TypeReplacement } from "../types";
import { createParameterName } from "./createParameterName";

export function multiFieldReplacement(type: InterfaceModel, field: FieldModel, path: string): TypeReplacement {
  return {
    replacement: type.id,
    type: field.fieldType.importedTypes[0].id,
    resolve: `${path}.{name}`,
    parameterName: createParameterName(type.id)
  };
}

export function fieldReplacement(type: InterfaceModel, field: FieldModel, path: string, parsed: ParsedModel[]): TypeReplacement {
  return {
    replacement: type.id,
    type: getOriginalId(field.fieldType.importedTypes[0].id, parsed),
    resolve: path,
    parameterName: createParameterName(type.id)
  };
}
