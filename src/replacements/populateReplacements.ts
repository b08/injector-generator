import { flatMap } from "@b08/array";
import { groupBy } from "@b08/flat-key";
import { FieldModel, InterfaceModel } from "@b08/type-parser";
import { getInterfaceByType, getOriginalId, interfaces, isImportableMonotype } from "@b08/type-parser-methods";
import { ParsedModel, TypeReplacement } from "../types";
import { fieldReplacement, multiFieldReplacement } from "./fieldReplacement";
import { getImplementationReplacements, getMockReplacements } from "./getImplementationReplacements";

export function populateTypeReplacements(parsed: ParsedModel[]): TypeReplacement[] {
  const replacementGroups = interfaces(parsed)
    .map(t => populateReplacementsForType(t, t, "", parsed));
  const sorted = replacementGroups.sort((g1, g2) => g2.length - g1.length);
  return [...flatMap(sorted), ...getImplementationReplacements(parsed), ...getMockReplacements(parsed)];
}

function populateReplacementsForType(type: InterfaceModel, current: InterfaceModel, path: string, parsed: ParsedModel[]): TypeReplacement[] {
  const fields = current.fields.filter(f => isImportableMonotype(f.fieldType));
  const grouped = groupBy(fields, f => getOriginalId(f.fieldType.importedTypes[0].id, parsed));
  return flatMap(grouped.values(), f => populateReplacementForFields(type, f, path, parsed));
}

function populateReplacementForFields(type: InterfaceModel, fields: FieldModel[], path: string, parsed: ParsedModel[]): TypeReplacement[] {
  if (fields.length > 1) { return [multiFieldReplacement(type, fields[0], path)]; }
  const subPath = `${path}.${fields[0].fieldName}`;
  const curType = getInterfaceByType(fields[0].fieldType, parsed);
  const replacement = fieldReplacement(type, fields[0], subPath, parsed);
  return curType == null
    ? [replacement]
    : [replacement, ...populateReplacementsForType(type, curType, subPath, parsed)];
}
