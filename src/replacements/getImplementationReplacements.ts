import { ClassModel, TypeDefinition } from "@b08/type-parser";
import { getOriginalId, isImportableMonotype } from "@b08/type-parser-methods";
import { classesToInject, getClassByType } from "../createInjectors/classesMap";
import { ParsedModel, TypeReplacement } from "../types";

export function getImplementationReplacements(parsed: ParsedModel[]): TypeReplacement[] {
  return classesToInject(parsed)
    .filter(cls => isImplementation(cls))
    .map(cls => createReplacement(cls, cls.implements[0], parsed));
}

export function getMockReplacements(parsed: ParsedModel[]): TypeReplacement[] {
  return classesToInject(parsed)
    .filter(cls => isMock(cls, parsed))
    .map(cls => createReplacement(cls, cls.extends[0], parsed));
}

function isImplementation(cls: ClassModel): boolean {
  return cls.implements.length === 1 && isImportableMonotype(cls.implements[0]);
}

function isMock(cls: ClassModel, parsed: ParsedModel[]): boolean {
  return cls.extends.length === 1 && isImportableMonotype(cls.extends[0]) &&
    getClassByType(cls.extends[0], parsed) != null &&
    cls.id.name === `${cls.extends[0].typeName}Mock`;
}

function createReplacement(cls: ClassModel, src: TypeDefinition, parsed: ParsedModel[]): TypeReplacement {
  return {
    type: getOriginalId(src.importedTypes[0].id, parsed),
    replacement: cls.id,
    resolve: ""
  };
}
