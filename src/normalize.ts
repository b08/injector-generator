import { TypeLimit, TypeReplacement, NameReplacement, ReplacementType, InjectionReplacement } from "./types";
import { resolve } from "path";
import { createParameterName } from "./replacements/createParameterName";

export function normalizeLimits(limits: TypeLimit[]): TypeLimit[] {
  return limits.map(l => ({ ...l, type: normalizeType(l.type) }));
}

export function normalizeTypeReplacements(replacements: TypeReplacement[] = []): TypeReplacement[] {
  return replacements.map(r => ({ ...normalizeNameReplacement(r), type: normalizeType(r.type) }));
}

export function normalizeNameReplacements(replacements: NameReplacement[] = []): NameReplacement[] {
  return replacements.map(normalizeNameReplacement);
}

function normalizeNameReplacement<T extends InjectionReplacement>(replacement: T): T {
  return {
    ...replacement,
    replacement: normalizeType(replacement.replacement),
    resolve: replacement.resolve || "",
    parameterName: replacement.parameterName || createParameterName(replacement.replacement)
  };
}

function normalizeType(id: ReplacementType): ReplacementType {
  return { ...id, folder: resolveFolder(id.folder) };
}
function resolveFolder(folder: string): string {
  return resolve(folder).replace(/[\\\/]+/g, "/");
}
