import { parseTypesInFiles } from "@b08/type-parser";
import { createInjectors } from "./createInjectors/createInjectors";
import { InjectorOptions } from "./createInjectors/types";
import { normalizeLimits, normalizeNameReplacements, normalizeTypeReplacements } from "./normalize";
import { produceInjectorFiles } from "./produce/produceInjectorFiles";
import { populateNameReplacements } from "./replacements/populateNameReplacements";
import { populateTypeReplacements } from "./replacements/populateReplacements";
import { ContentFile, GeneratorOptions, ParsedModel } from "./types";

const defaultOptions: GeneratorOptions = {
  quotes: "\"",
  linefeed: "\n",
  aliasMap: {},
  limits: [],
  typeReplacements: [],
  nameReplacements: []
};

export function generateInjectors<T extends ContentFile>(files: T[], options: GeneratorOptions = {}): T[] {
  options = {
    ...defaultOptions,
    ...options,
  };
  const filtered = files.filter(x => x.extension === ".ts");
  const parsed = parseTypesInFiles(filtered, options);
  options.typeReplacements = [...normalizeTypeReplacements(options.typeReplacements), ...populateTypeReplacements(parsed)];
  options.nameReplacements = [...normalizeNameReplacements(options.nameReplacements), ...populateNameReplacements(parsed)];
  options.limits = normalizeLimits(options.limits);

  const injectors = createInjectors(createInjectorOptions(parsed, options));
  const result = produceInjectorFiles(injectors, options);
  return result.map(file => ({ ...files[0], ...file }));
}

function createInjectorOptions(parsed: ParsedModel[], options: GeneratorOptions): InjectorOptions {
  return {
    typeReplacements: options.typeReplacements, nameReplacements: options.nameReplacements, parsed, limits: options.limits
  };
}
