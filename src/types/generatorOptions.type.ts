import { IMap } from "@b08/object-map";

export type Quotes = "'" | "\"";
export type LineFeed = "\n" | "\r\n" | "\r";

export interface GeneratorOptions {
  linefeed?: LineFeed;
  quotes?: Quotes;
  aliasMap?: IMap; // type-parser option
  limits?: TypeLimit[];
  typeReplacements?: TypeReplacement[];
  nameReplacements?: NameReplacement[];
}

export interface TypeLimit {
  type: ReplacementType;
  limitToPath: RegExp;
}

export interface TypeReplacement extends InjectionReplacement {
  type: ReplacementType; // type to replace
}

export interface NameReplacement extends InjectionReplacement {
  typeNameRegex: RegExp; // match typename to replace
}

export interface InjectionReplacement {
  resolve?: string; // dependency path from replacement will be resolved with this
  replacement: ReplacementType; // replace matched type with this type
  parameterName?: string; // new parameter name
}

export interface ReplacementType {
  folder: string;
  file: string;
  name: string;
  isModulesPath: boolean;
}
