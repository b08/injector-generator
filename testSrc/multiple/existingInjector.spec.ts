import { test } from "@b08/test-runner";
import { createWorld } from "../createWorld";

test("two services, existing injector", async expect => {
  // arrange
  const srcFile = "../../testData/twoFoldersExistingInjector/injector";
  const target = await import(srcFile);
  const world = createWorld();

  // act
  const result = target.firstService(world).name();

  // assert
  expect.equal(result, "FirstService SecondService dbStr");
});
