import { test } from "@b08/test-runner";

test("long dependency list", async expect => {
  // arrange
  const srcFile = "../../testData/longDependencyList/injector";
  const target = await import(srcFile);

  // act
  const result = target.firstService().name();

  // assert
  expect.equal(result, "123456verylong");
});
