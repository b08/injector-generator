import { test } from "@b08/test-runner";

test("two services, singletonism", async expect => {
  // arrange
  const srcFile = "../../testData/singletonism/injector";
  const target = await import(srcFile);

  // act
  const result = target.mainService().same();

  // assert
  expect.true(result);
});
