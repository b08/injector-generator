import { test } from "@b08/test-runner";
import { createWorld } from "../createWorld";

test("two services, reference by bucket", async expect => {
  // arrange
  const srcFile = "../../testData/bucketReference/injector";
  const target = await import(srcFile);
  const world = createWorld();
  // act
  const result = target.mainBucketService(world).name();

  // assert
  expect.equal(result, "Main Child dbStr");
});
