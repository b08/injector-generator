import { test } from "@b08/test-runner";
import { createWorld } from "../createWorld";

test("two services, different rules, main service", async expect => {
  // arrange
  const srcFile = "../../testData/twoFoldersDifferentRules/injector";
  const target = await import(srcFile);
  const world = createWorld();

  // act
  const result1 = target.firstService(world).name1();
  const result2 = target.firstService(world).name2();

  // assert
  expect.equal(result1, "FirstService SecondService dbStr");
  expect.equal(result2, "FirstService ThirdService dbStr");
});

test("two services, different rules", async expect => {
  // arrange
  const srcFile = "../../testData/twoFoldersDifferentRules/otherRules/injector";
  const target = await import(srcFile);
  const world = createWorld();

  // act
  const result = target.secondService(world.backend).name();

  // assert
  expect.equal(result, "SecondService dbStr");
});
