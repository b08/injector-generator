import { test } from "@b08/test-runner";
import { createWorld } from "../createWorld";

test("auto mock", async expect => {
  // arrange
  const srcFile = "../../testData/autoMock/injector";
  const target = await import(srcFile);
  const world = createWorld();

  // act
  const result = target.mainService(world).name();

  // assert
  expect.equal(result, "Main ChildMock dbStr");
});
