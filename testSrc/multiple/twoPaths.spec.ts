import { test } from "@b08/test-runner";
import { createWorld } from "../createWorld";

test("three services, two paths", async expect => {
  // arrange
  const srcFile = "../../testData/threeServicesTwoPaths/injector";
  const target = await import(srcFile);
  const world = createWorld();

  // act
  const result1 = target.firstService(world).firstName();
  const result2 = target.firstService(world).secondName();

  // assert
  expect.equal(result1, "FirstService SecondService beStr");
  expect.equal(result2, "ThirdService SecondService beStr");
});
