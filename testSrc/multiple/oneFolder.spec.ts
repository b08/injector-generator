import { test } from "@b08/test-runner";
import { createWorld } from "../createWorld";

test("two services one folder", async expect => {
  // arrange
  const srcFile = "../../testData/twoServicesOneFolder/injector";
  const target = await import(srcFile);
  const world = createWorld();

  // act
  const result = target.secondService(world).name();

  // assert
  expect.equal(result, "SecondService FirstService dbStr");
});
