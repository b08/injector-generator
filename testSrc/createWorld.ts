export function createWorld(beStr: string = "beStr", dbStr: string = "dbStr"): any {
  return {
    backend: {
      beStr,
      db: { dbStr }
    }
  };
}
