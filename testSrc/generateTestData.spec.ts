import { MultiFileConverter, transformRange } from "@b08/gulp-transform";
import { dest, src } from "gulp";
import { generateInjectors, GeneratorOptions, ReplacementType } from "../src";
import * as fse from "fs-extra";

const worldType: ReplacementType = { file: "world.type", folder: "./testData", name: "IWorld", isModulesPath: false };

const contextType: ReplacementType = { file: "context.type", folder: "./testData/nameReplacement", name: "IContext", isModulesPath: false };

const options: GeneratorOptions = {
  limits: [{ type: worldType, limitToPath: /^((?!otherRules).)+$/ }],
  nameReplacements: [{ typeNameRegex: /^User\d$/, replacement: contextType, resolve: ".user" }]
};

generateTestData(files => generateInjectors(files, options))
  .catch(err => {
    console.error(err);
    process.exitCode = -1;
  });

async function generateTestData(generator: MultiFileConverter): Promise<void> {
  let folders = await fse.readdir("./testData");
  await Promise.all(folders.map(folder => generateInFolder(folder, generator)));
}

function generateInFolder(folder: string, generator: MultiFileConverter): Promise<void> {
  const globs = [`./testData/${folder}/**/*.@(service|type|mock).ts`, "./testData/world.type.ts", "./testData/**/index.ts"];
  const stream = src(globs)
    .pipe(transformRange(files => generator(files)))
    .pipe(dest(`./testData/${folder}`));

  return waitForStream(stream);
}

export function waitForStream(stream: NodeJS.ReadWriteStream): Promise<void> {
  return new Promise((resolve, reject) => {
    stream.on("error", reject);
    stream.on("end", resolve);
  });
}
