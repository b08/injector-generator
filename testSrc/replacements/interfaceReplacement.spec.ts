import { test } from "@b08/test-runner";
import { createWorld } from "../createWorld";

test("two services, interface parameter is replaced by implementing class", async expect => {
  // arrange
  const srcFile = "../../testData/interfaceReplacement/injector";
  const target = await import(srcFile);
  const world = createWorld();
  // act
  const result = target.topService(world).name();

  // assert
  expect.equal(result, "Top Some dbStr");
});
