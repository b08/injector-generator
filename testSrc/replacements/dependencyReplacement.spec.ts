import { test } from "@b08/test-runner";

test("two services, dependency is replaced", async expect => {
  // arrange
  const srcFile = "../../testData/dependencyReplacement/injector";
  const target = await import(srcFile);

  // act
  const result = target.mainService().name();

  // assert
  expect.equal(result, "ChildMock");
});
