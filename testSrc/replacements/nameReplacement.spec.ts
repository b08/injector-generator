import { test } from "@b08/test-runner";

test("two services, replacement by name", async expect => {
  // arrange
  const srcFile = "../../testData/nameReplacement/injector";
  const target = await import(srcFile);
  const context = { user: { name: "User" } };
  // act
  const result = target.mainService(context).name();

  // assert
  expect.equal(result, "Child1UserChild2User");
});
