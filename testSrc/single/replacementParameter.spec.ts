import { test } from "@b08/test-runner";
import { createWorld } from "../createWorld";

test("single replacement parameter", async expect => {
  // arrange
  const srcFile = "../../testData/singleReplacement/injector";
  const target = await import(srcFile);
  const world = createWorld();

  // act
  const result = target.replacementParameterService(world).name();

  // assert
  expect.equal(result, "ReplacementParameterService beStr");
});
