import { test } from "@b08/test-runner";

test("single one parameter", async expect => {
  // arrange
  const srcFile = "../../testData/singleOneParameter/injector";
  const target = await import(srcFile);

  // act
  const result = target.oneParmService("a").name();

  // assert
  expect.equal(result, "OneParmService a");
});
