import { test } from "@b08/test-runner";

test("single no parameter", async expect => {
  // arrange
  const srcFile = "../../testData/singleNoParameters/injector";
  const target = await import(srcFile);

  // act
  const result = target.noParmService().name();

  // assert
  expect.equal(result, "NoParmService");
});
