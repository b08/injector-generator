import { test } from "@b08/test-runner";

test("type reference by index", async expect => {
  // arrange
  const srcFile = "../../testData/typeRefByIndex/injector";
  const target = await import(srcFile);
  const context = {
    model: {
      name: "name"
    }
  };

  // act
  const result = target.firstService(context).name();

  // assert
  expect.equal(result, "First name");
});
