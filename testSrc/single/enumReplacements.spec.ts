import { test } from "@b08/test-runner";

test("single one parameter", async expect => {
  // arrange
  const srcFile = "../../testData/enumReplacements/injector";
  const target = await import(srcFile);
  const parameter = { a: "val1", b: "val2" };
  // act
  const result = target.mainService(parameter).name();

  // assert
  expect.equal(result, "val1val2");
});
