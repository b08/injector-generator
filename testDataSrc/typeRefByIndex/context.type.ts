import { Model } from "./model";

export interface Context {
  model: Model;
}
