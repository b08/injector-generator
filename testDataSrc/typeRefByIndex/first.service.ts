import { Model } from "./model";

export class FirstService {
  constructor(private model: Model) { }

  public name(): string {
    return "First " + this.model.name;
  }
}
