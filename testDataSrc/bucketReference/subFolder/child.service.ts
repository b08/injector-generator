import { IDb } from "../../world.type";

export class ChildService {
  constructor(private db: IDb) { }

  public name(): string {
    return "Child " + this.db.dbStr;
  }
}
