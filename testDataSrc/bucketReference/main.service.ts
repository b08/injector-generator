import { ChildService } from "./subFolder";

export class MainBucketService {
  constructor(private child: ChildService) { }

  public name(): string {
    return "Main " + this.child.name();
  }
}
