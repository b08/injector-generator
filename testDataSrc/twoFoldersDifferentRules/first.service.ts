import { SecondService } from "./otherRules/second.service";
import { ThirdService } from "./subFolder/third.service";

export class FirstService {
  constructor(private second: SecondService, private third: ThirdService) {
  }

  public name1(): string {
    return "FirstService " + this.second.name();
  }

  public name2(): string {
    return "FirstService " + this.third.name();
  }
}
