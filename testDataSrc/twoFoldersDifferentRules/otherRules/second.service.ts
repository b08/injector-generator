import { IDb } from "../../world.type";

export class SecondService {
  constructor(private db: IDb) {
  }

  public name(): string {
    return "SecondService " + this.db.dbStr;
  }
}
