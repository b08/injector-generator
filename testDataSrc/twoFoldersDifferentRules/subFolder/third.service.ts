import { IDb } from "../../world.type";

export class ThirdService {
  constructor(private db: IDb) {
  }

  public name(): string {
    return "ThirdService " + this.db.dbStr;
  }
}
