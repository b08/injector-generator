export class OneParmService {
  constructor(private str: string) { }

  public name(): string {
    return "OneParmService " + this.str;
  }
}
