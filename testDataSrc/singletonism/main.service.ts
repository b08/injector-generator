export class MainService {
  constructor(private child1: ChildService, private child2: ChildService) { }

  public same(): boolean {
    return this.child1 === this.child2;
  }
}

export class ChildService { }
