import { IBeContext, IDb } from "../../world.type";

export class SecondService {
  constructor(private db: IDb) { }

  public name(): string {
    return "SecondService " + this.db.dbStr;
  }
}

export function secondService(context: IBeContext, instances: any): SecondService {
  const existing = instances["secondServiceInjector"];
  if (existing != null) { return existing; }
  return instances["secondServiceInjector"] = new SecondService(context.db);
}
