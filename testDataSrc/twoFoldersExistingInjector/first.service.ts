import { SecondService } from "./subFolder/second.service";

export class FirstService {
  constructor(private second: SecondService) { }

  public name(): string {
    return "FirstService " + this.second.name();
  }
}
