import { User1, User2 } from "./context.type";

export class Child1Service {
  constructor(private user: User1) { }

  public name(): string {
    return "Child1" + this.user.name;
  }
}

export class Child2Service {
  constructor(private user: User2) { }

  public name(): string {
    return "Child2" + this.user.name;
  }
}
