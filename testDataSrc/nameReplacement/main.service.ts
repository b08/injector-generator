import { Child1Service, Child2Service } from "./child.service";

export class MainService {
  constructor(private child1: Child1Service, private child2: Child2Service) { }

  public name(): string {
    return this.child1.name() + this.child2.name();
  }
}
