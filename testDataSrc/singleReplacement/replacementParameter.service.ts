import { IBeContext } from "../world.type";

export class ReplacementParameterService {
  constructor(private backend: IBeContext) { }

  public name(): string {
    return "ReplacementParameterService " + this.backend.beStr;
  }
}
