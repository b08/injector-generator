import { ThirdService, FourthService, FifthService, SixthService } from "./moar.service";
import { SecondService } from "./second.service";
import { VeryLongNameVeryLongNameVeryLongNameVeryLongService } from "./veryLongName.service";

export class FirstService {
  constructor(private third: ThirdService, private fourth: FourthService,
    private longName: VeryLongNameVeryLongNameVeryLongNameVeryLongService,
    private second: SecondService, private fifth: FifthService, private sixth: SixthService) { }

  public name(): string {
    return "1" + this.second.name() + this.third.name() + this.fourth.name()
      + this.fifth.name() + this.sixth.name() + this.longName.name();
  }
}
