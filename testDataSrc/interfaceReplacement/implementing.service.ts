import { ISomeService } from "./iface";
import { IDb } from "../world.type";

export class SomeService implements ISomeService {
  constructor(private db: IDb) { }

  public name(): string {
    return "Some " + this.db.dbStr;
  }
}
