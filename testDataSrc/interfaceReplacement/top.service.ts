import { ISomeService } from "./iface";

export class TopService {
  constructor(private some: ISomeService) { }

  public name(): string {
    return "Top " + this.some.name();
  }
}
