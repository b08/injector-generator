export interface IWorld {
  backend: IBeContext;
}

export interface IBeContext {
  db: IDb;
  beStr: string;
}

export interface IDb {
  dbStr: string;
}

