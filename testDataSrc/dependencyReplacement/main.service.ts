export class MainService {
  constructor(private child: ChildService) { }

  public name(): string {
    return this.child.name();
  }
}

export class ChildService {
  public name(): string {
    return "ChildService";
  }
}
