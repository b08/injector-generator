import { FirstService } from "./first.service";

export class SecondService {
  constructor(private first: FirstService) { }

  public name(): string {
    return "SecondService " + this.first.name();
  }
}
