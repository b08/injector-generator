import { IDb } from "../world.type";

export class FirstService {
  constructor(private db: IDb) { }

  public name(): string {
    return "FirstService " + this.db.dbStr;
  }
}
