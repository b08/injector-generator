export enum Enum1 {
  val1 = "val1"
}

export enum Enum2 {
  val2 = "val2"
}


export interface Parameters {
  a: Enum1;
  b: Enum2;
}

export class MainService {
  constructor(private e1: Enum1, private e2: Enum2) { }

  public name(): string {
    return this.e1 + this.e2;
  }
}
