import { ChildService } from "./child.service";

export class MainService {
  constructor(private child: ChildService) { }

  public name(): string { return "Main " + this.child.name(); }
}
