import { ChildService } from "./child.service";
import { IDb } from "../world.type";

export class ChildServiceMock extends ChildService {

  constructor(private db: IDb) { super(); }
  public name(): string { return "ChildMock " + this.db.dbStr; }
}
