import { IBeContext } from "../../world.type";

export class SecondService {
  constructor(private backend: IBeContext) { }

  public name(): string {
    return "SecondService " + this.backend.beStr;
  }
}
