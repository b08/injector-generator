import { SecondService } from "./subFolder/second.service";

export class ThirdService {
  constructor(private second: SecondService) { }

  public name(): string {
    return "ThirdService " + this.second.name();
  }
}
