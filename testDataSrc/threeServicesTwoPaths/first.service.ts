import { SecondService } from "./subFolder/second.service";
import { ThirdService } from "./third.service";

export class FirstService {
  constructor(private second: SecondService, private third: ThirdService) { }

  public firstName(): string {
    return "FirstService " + this.second.name();
  }

  public secondName(): string {
    return this.third.name();
  }
}
